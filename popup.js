chrome.tabs.query({'active': true, 'windowId': chrome.windows.WINDOW_ID_CURRENT},
	function(tabs) {
		const url = new URL(tabs[0].url);
		var wrapper = document.createElement('DIV');
		for (pair of url.searchParams) {
			const foo = document.createElement('DIV');
			foo.classList.add('queryVariableContainer');
			var foobarElement = document.createElement('SPAN');
			foobarElement.classList.add('variableSpan');
			foobarElement.innerText = `${pair[0]}`;
			var fooElement = document.createElement('INPUT');
			fooElement.classList.add('variableValue');
			fooElement.setAttribute('value', pair[1]);
			foo.appendChild(foobarElement);
			foo.appendChild(fooElement);
			wrapper.appendChild(foo);
		}
		if (!wrapper.children.length) {
			wrapper.innerText = 'error';
		}
		document.body.appendChild(wrapper);
		//update url with changing value of variable
		wrapper.addEventListener('keyup', function() {
			if (event.key === 'Enter') {
				// url.href = url.host + url.pathname + '?' + event.target.previousSibling.innerText + '=' + event.target.value;
				// alert(url.href);
				const queryVariables = document.getElementsByClassName('queryVariableContainer');
				var queryString = '';
				[...queryVariables].forEach((element, number) => {
					var children = element.childNodes;
					for (var i = 0; i < children.length; ++i) {
						if (children[i].classList.contains('variableSpan')) {
							queryString = queryString + children[i].innerText + '=';
						} else if (children[i].classList.contains('variableValue')) {
							queryString = queryString + children[i].value;
						}
						if ((children.length >= 2) && (i == 1) && (number < queryVariables.length - 1)) {
							queryString = queryString + '&';
						}
					}
				});
				var newUrl = url.host + url.pathname + '?' + queryString;
				alert(newUrl);
			}
			//update url through GET req
			//chrome.tabs.update(tabs.id, {url: url.href});
		});
	}
);